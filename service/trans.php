<?php
function trans($key)
{
	$trans = json_decode(file_get_contents(ROOT. '/app/translate.json'),1);

	$arr = explode('.',$key);

	foreach($arr as $val)
	{
		$trans = $trans[$val];
	}

	return $trans == null ? $key ? $trans;
}
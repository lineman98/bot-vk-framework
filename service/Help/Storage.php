<?php
namespace Service\Help;

class Storage
{
	public function __construct()
	{
		$dir = scandir(root.'/storage');

		foreach($dir as $filename)
		{
			$name = preg_replace('/\.json/','',$filename);
			if($filename  == '.' or $filename == '..' or (!file_exists(root.'/storage/'.$filename))) continue;
			
			$info = json_decode(file_get_contents(root.'/storage/'.$filename),1);
			$this->{$name} = $info;

			
		}
	}

	public function save()
	{
		$dir = scandir(root.'/storage');
		foreach($dir as $filename)
		{
			$name = preg_replace('/\.json/','',$filename);

			if(property_exists($this, $name))
			{
				$info = json_encode($this->{$name},JSON_PRETTY_PRINT+JSON_UNESCAPED_UNICODE);
				file_put_contents(root.'/storage/'.$filename, $info);
			}
		}
	}
}
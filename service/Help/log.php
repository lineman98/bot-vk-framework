<?php

function log($msg)
{
	$log = file_get_contents('log.txt');
	$log .= "\n----------------- " . date('Y.m.d H:i:s') . "\n" . $msg;
	file_put_contents('log.txt', $log);
}
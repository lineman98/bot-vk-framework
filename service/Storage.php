<?php
namespace Service;

class Storage
{
	public function __construct()
	{
		foreach($this->loadStorage() as $key => $value)
		{
			$this->{$key} = $value;
		}
	}

	public function save($folder = '', $scan = '')
	{
		if($folder == '') $folder = ROOT . '/storage';
		if($scan == '') $scan = $this;

		$toScan = is_object($scan) ? get_object_vars($scan) : $scan;
		foreach($toScan as $varName => $varValue)
		{
			if($varName == '.' or $varName == '..') continue;

			$filepath = $folder. '/' .$varName;
			
			if(file_exists($filepath.'.json'))
			{
				if(json_encode($varValue) !== null)
					$this->writeFile($filepath . '.json', json_encode($varValue,JSON_PRETTY_PRINT+JSON_UNESCAPED_UNICODE));
			}
			else
			{
				$this->save($filepath, $scan->{$varName});
			}
		}
	}

	public function loadStorage($folder = ROOT . '/storage')
	{
		$storage = [];

		$dir = scandir($folder);

		foreach($dir as $filename)
		{
			if($filename  == '.' or $filename == '..') continue;

			//полное имя файла
			$filepath = $folder . '/' . $filename;
			//отделяем от имени файла расширение .json
			$name = preg_replace('/\.json/','',$filename);
			
			if(file_exists($filepath))
			{
				if(!is_dir($filepath) )
				{
					$storage[$name] = json_decode($this->readFile($filepath),1);
				}
				else
				{
					$storage[$name] = $this->loadStorage($filepath);
				}
			}
			
		}

		return $storage;
	}


	private function readFile($filepath)
	{
		$file = fopen($filepath,'r');
		if(flock($file, LOCK_NB+LOCK_EX))
		{
			$content = fread($file,filesize($filepath));
		}
		fclose($file);

		return $content;
	}

	private function writeFile($filepath, $content)
	{
		$file = fopen($filepath,'w');
		$content = fwrite($file,$content);
		flock($file,LOCK_UN);
		fclose($file);
	}
}
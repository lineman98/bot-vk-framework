<?php
namespace Service;

class User
{
	private $ownStorage;

	public function __construct($user_id, & $storage = null)
	{
		if($storage == null)
		{
			$this->storage = new Storage;
			$this->ownStorage = true;
		}
		else
			$this->storage = & $storage;  
		$this->loadChatInfo($user_id);
		$this->loadUserinfo();

		$this->checkLoadedVkData();
	} 

	public function __destruct()
	{
		if($this->ownStorage == true)
		{
			$this->storage->save();
		}
	}

	public function loadChatInfo($user_id)
	{

		if(!array_key_exists($user_id, $this->storage->chats))
		{	
			$this->storage->chats[$user_id] = [
				'vk_id' => $user_id,
				'command' => 'StartCommand'
			];
		}

		foreach($this->storage->chats[$user_id] as $key => $val)
		{
			$this->{$key} = & $this->storage->chats[$user_id][$key];
		}
	}

	public function loadUserinfo()
	{
		if(property_exists($this, 'phone_number'))
		{
			if(array_key_exists($this->phone_number,$this->storage->userinfos))
			{
				$this->userinfo = & $this->storage->userinfos[$this->phone_number];
			}
		}
		else
			$this->userinfo = [];
	}

	public function __set($name,$value)
	{
		$this->{$name} = $value;
		$this->storage->chats[$this->vk_id]{$name} = $value;

		if($name == 'phone_number')
		{
			$this->loadUserinfo();
		}
	}

	protected function checkLoadedVkData()
	{
		//print_r(file_get_contents('https://api.vk.com/method/users.get?v=5.81&user_ids=240597953&access_token=5e4c97948848b606515494ab1e0c6dfabca46015bbff7af73840a7b52e46c5bb0236253adbba3d8c78630'));
		if(!property_exists($this,'name'))
		{
			$vk = new \VK\Client\VKApiClient('5.85');
			try{
				$userinfo = $vk->users()->get(config('group.access_token'),[
					'user_ids' => (string)$this->vk_id
				]);

				if(array_key_exists(0,$userinfo) && !empty($userinfo[0]))
				{
					$this->name = $userinfo[0]['first_name'];
					$this->surname = $userinfo[0]['last_name'];
				}
			}
			catch(\Exception $e)
			{
				print_r($e);
			}
		}
	}
}
<?php
/**
 * Возвращает прееменную из конфиг файлы, 
 * параметр передавать в точечном виде configFile.configVar[.innerVar...]
 */
function config($path)
{
	$path_array = explode('.',$path);
	$full_config = include('config/' . $path_array[0] . '.php');

	$config_var = $full_config;

	for($i = 1; $i < count($path_array); $i++)
	{
		$config_var = $config_var[$path_array[$i]];
	}

	return $config_var;
}

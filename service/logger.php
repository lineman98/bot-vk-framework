<?php

function logger($msg)
{
	$log = file_get_contents(ROOT.'/log.txt');
	$log .= "\n----------------- " . date('Y.m.d H:i:s') . "\n" . $msg;
	file_put_contents(ROOT.'/log.txt', $log);
}
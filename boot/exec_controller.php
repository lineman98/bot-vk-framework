<?php
$controllerName =  ucfirst( isset($_GET['path']) ? $_GET['path'] : 'Responder' );

$controller_fullname = 'App\\Http\\Controller\\' . $controllerName;

if(class_exists($controller_fullname))
{
	$instance = new $controller_fullname;
	print_r($instance->index());
}
<?php
use Models\SimpleORM;

//коннектимся к БД и делаем рабочими модели
$db = new mysqli(config('db.host'), config('db.user'), config('db.password'));
if ($db->connect_error)
  exit('Bad connetion with database : ' . $db->connect_error);
SimpleORM::useConnection($db, config('db.name'));

<?php
namespace App\Http\Command;

class BeforeFinalCheckUserinfo extends Command
{
    protected $message = '{{name}}, ураааа 🎉 

    Я получила всю нужную информацию, давайте проверим ее, а после приступим к вводной части 🔥
    
    Нажмите на кнопку “Проверить информацию” 💪';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            "name" => 'Проверить информацию',
            'color' => 'blue',
            'goto_command' => 'FinalCheckUserinfo'
        ]
    ];
}
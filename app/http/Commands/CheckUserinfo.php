<?php
namespace App\Http\Command;

class CheckUserinfo extends Command
{
    protected $message = '{{ name }}, мы нашли вашу заявку ✅

    Вот информация, которую мы нашли о вас: 👇 ';
    protected $attachment = '';
 
    protected $keyboard_one_time = false;

    protected $buttons_row_1 = [
        [
            'name' => 'Да, это я',
            'color' => 'зеленый',

            'text_payload' => 'Да',
            'goto_command' => 'ReadyToLoadPhotos'
        ],
        [
            'name' => 'Нет, это не я',
            'color' => 'красный',

            'text_payload' => 'нет',
            'goto_command' => 'IncorrectUserinfo'
        ]
    ];

    public function preHandler()
    {
        $this->message .= "<br>";
        foreach($this->user->userinfo['constant'] as $key => $val)
        {
            if(is_string($val) == false) continue;

            $key = ucfirst($key);
            $val = ucfirst($val);
            $this->message .= "<br>$key: $val";
        }
        $this->message .= '<br><br> Это вы?';
    }

    public function handler()
    {
        parent::handler();

        if(mb_ereg_match('Да, это я',$this->input->object->text,'i'))
            $this->user->correctUserinfo = true;
        elseif(mb_ereg_match('Нет, это не я',$this->input->object->text,'i'))
            $this->user->correctUserinfo = false;
    }
}
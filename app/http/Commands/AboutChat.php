<?php
namespace App\Http\Command;

class AboutChat extends Command
{
    protected $message = '{{name}}, сейчас нам надо подключиться к закрытому чату &#128274;  

Для этого, нажми вот сюда: &#128071;
https://vk.me/join/AJQ1dxzskQnmBhvgFuzEa5OF 

{{name}}, когда вы присоединитесь к чату, нажмите на кнопку "Идем дальше"';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Идем дальше',
            'color' => 'green',
            'goto_command' => 'AboutRecomendations'
        ] 
    ];
}
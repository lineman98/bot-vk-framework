<?php
namespace App\Http\Command;

class SendPhotos extends Command
{
    protected $message = '{{ name }}, прикрепите ваши фотографии и отправьте сообщение 👍';
    protected $attachment = '';


    public function handler()
    {
        if(array_key_exists('attachments',$this->input->object))
        {
            $atts = [];
            foreach($this->input->object->attachments as $att)
            {
                if($att->type != 'photo') continue;

                $max_rezolution = null;
                $max_resol_url = null;
                foreach($att->photo->sizes as $size)
                {
                    if($size->width * $size->height > $max_rezolution)
                    {
                        $max_resolution = $size->width * $size->height;
                        $max_resol_url = $size->url;
                    }
                }

                $atts[] = $max_resol_url;
            }
            if(count($atts) == 3)
            {
                $this->user->userinfo['getted']['photos'] = $atts;
                $this->nextCommand = 'ReadyToSendDimensions';
            }
        }
    
    }
}
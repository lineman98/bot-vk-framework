<?php
namespace App\Http\Command;

class IncorrectUserinfo extends Command
{
    protected $message = '{{name}}, похоже мои создатели напортачили 🙆 

"Попробовать еще раз" - ввести повторно номер телефона. 
"Нужна помощь" - если Фити не может найти вашу заявку.';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Попробовать еще раз',
            'color' => 'blue',

            'text_payload' => 'попробовать',
            'goto_command' => 'PhoneNumber'
        ]
    ];

    protected $buttons_row_2 = [
        [
            'name' => 'Нужна помощь',
            'color' => 'green',

            'text_payload' => 'помощь',
            'goto_command' => 'CheckUserinfoHelp'
        ]
    ];

    public function handler()
    {
        parent::handler();
    }
}
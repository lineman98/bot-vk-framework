<?php
namespace App\Http\Command;

class SetTarget extends Command
{
    protected $message = '{{name}}, ваш уровень – {{Уровень}} ✅

    Осталось ответить на один важный вопрос 👇
    Ваш желаемый результат в процессе прохождения проекта? 
    
    Это может быть -2 кг или любое другое условие, которое вам подходит 💪
    
    Напишите в сообщении свой ответ в свободной форме, чего вы хотите достичь за месяц упорной работы🏃‍♀️';
    protected $attachment = '';


    public function handler()
    {
    	if(!empty($this->input->object->text))
    	{
    		$this->user->userinfo['getted']['Цель на мес'] = $this->input->object->text;

        	$this->nextCommand = 'BeforeFinalCheckUserinfo';
    	}


        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
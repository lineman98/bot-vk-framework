<?php
namespace App\Http\Command;

class FormFinal extends Command
{
    protected $message = '{{name}}, поздравляю вас с первым выполненным заданием! 👍 

    Давайте начнем наше маленькое обучение? 
    
    Нажмите на "Готова" 💪';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Готова',
            'color' => 'green',
            'goto_command' => 'TourStart'
        ] 
    ];


    public function preHandler()
    {
    	if(array_key_exists('system',$this->user->userinfo))
    		$this->user->userinfo['system'] = [];
    	
    	$this->user->userinfo['system']['active'] = true;
    	$this->user->userinfo['system']['start_time'] = time();
        $this->user->userinfo['system']['day'] = 0;
        $this->user->userinfo['system']['vk_id'] = $this->input->object->from_id;
    }
 
}
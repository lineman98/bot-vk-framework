<?php
namespace App\Http\Command;

class ReadyToSendLengths extends Command
{
    protected $message = '{name}, отлично, напиши в виде списка, который я тебе только что отсылал свои замеры. Пример:
    1) 140см
    2) 30см ...';
    protected $attachment = '';



    public function handler()
    {
        parent::handler();

        $this->nextCommand = 'SelectLevel';
    
    }
}
<?php
namespace App\Http\Command;

use VK\Client\VKApiClient;

class Command
{

	protected $message = '';
	protected $attachment = '';
	protected $keyboard = [];
	protected $keyboard_one_time = true;

	protected $nextCommand = '';
	

	public function __construct(&$user, &$input, &$storage)
	{
		$this->user = & $user;
		$this->input = & $input;
		$this->storage = & $storage;

		$this->vk = new VKApiClient(5.85);
	}


	/**
	 * Возвращает данные для ответ пользователю
	 */
	public function getAnswer()
	{
		$this->preHandler();

		$answer = [];
		$this->matchMessage();
		$answer['message'] = mb_ereg_replace("\r|\n[ \t]*","\r\n",str_replace("\t",'',$this->message));
		$answer['attachment'] = $this->attachment;

		$keyboard = $this->getGeneratedKeyboard();
		if($keyboard != false) $answer['keyboard'] = $keyboard;

		return $answer;
	}

	/**
	 * Возвращает какую команду вызвать следующей
	 */
	public function getNextCommand()
	{
		if(!empty($this->nextCommand))
		{
			if(class_exists('App\\Http\\Command\\' . $this->nextCommand)) 
			{
				return $this->nextCommand;
			}
		}
	}
	/**
	 * Обрабатывает ответ пользователя
	 */
	public function handler()
	{
		if(!empty($this->input->object->payload))
		{
			$payload = json_decode($this->input->object->payload);
			if(!empty($payload->goto_command))

			$this->nextCommand = $payload->goto_command;
		}
		else
		{
			foreach(get_object_vars($this) as $varName => $varValue)
			{
				if(preg_match('/buttons_row_(\d+)/',$varName,$preg_result))
				{
					foreach($varValue as $button)
					{
						if(array_key_exists('text_payload',$button))
						{
							if(mb_ereg_match($button['text_payload'],$this->input->object->text,'i'))
							{
								$this->nextCommand = $button['goto_command'];
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Вызываетс перед отсылкой сообщения текущей команды
	 */
	public function preHandler()
	{
		return 1;
	}
	


	/**
	 * Возвращает клавиатуру в формате нужном вк
	 */
	protected function getGeneratedKeyboard()
	{
		$keyboard = [
			'one_time' => false,
			'buttons' => []
		];

		$vars = get_object_vars($this); 
		foreach($vars as $varName => $varValue)
		{
			if(preg_match('/buttons_row_(\d+)/',$varName,$preg_result))
			{
				$row = [];
				//преобразуем кнопкu в нужный вид
				foreach($varValue as $button)
				{
					$payload = json_encode([
						'goto_command' => $button['goto_command']  
					],JSON_UNESCAPED_UNICODE);

					$row[] = [
						'action' => [
							'type' => 'text',
							'payload' => $payload,
							'label' => $button['name']
						],
						'color' => $this->colorToEng($button['color'])
					];
				}
				$keyboard['buttons'][] = $row; 
			}
		}
		$keyboard['one_time'] = $this->keyboard_one_time;
		
		if(count( $keyboard['buttons'] ) > 0) 
		{
			return json_encode($keyboard, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT);
		}
		else
		{
			return false;
		}
	}

	protected function colorToEng($color)
	{
		$colors = [
			'зеленый' => 'positive',
			'красный' => 'negative',
			'белый' => 'default',
			'синий' => 'primary',
			'green' => 'positive',
			'red' => 'negative',
			'white' => 'default',
			'blue' => 'primary'
		];
		foreach($colors as $color_rus => $color_eng)
		{
			if(preg_match('/' . $color_rus . '/i',$color))
			{
				return $color_eng;
			}
		}
	}

	protected function matchMessage()
	{
		
		preg_match_all('/\{\{(.+)\}\}/iU',$this->message,$result);
		
		foreach($result[1] as $word)
		{
			$word = trim($word);

			$val = null;
			if(property_exists($this->user,$word))
			{
				$val = $this->user->{$word};
			}
			elseif(array_key_exists($word, $this->user->userinfo))
			{
				$val = $this->user->userinfo[$word];
			}
			elseif(array_key_exists($word, $this->user->userinfo['getted']))
			{
				$val = $this->user->userinfo['getted'][$word];
			}
			elseif(array_key_exists($word, $this->user->userinfo['constant']))
			{
				$val = $this->user->userinfo['constant'][$word];
			}
			
			if($val != null)
			{
				$this->message = preg_replace('/\{\{[ ]*'. $word .'[ ]*\}\}/Ui', $val,$this->message);
			}
		}  
	}



	protected function sendReports()
	{
		$owner_ids = config('group.admins');

		$msg = 'У пользователя http://vk.com/id'. $this->user->vk_id . ' какие-то проблемы. Пожалуйста, немедленно помогите ему :)';
		$code = '
			var user_ids = ' . json_encode($owner_ids) . ';
			var i = 0;
			while(i < user_ids.length)
			{
				API.messages.send({
					"message": "' . $msg . '",
					"user_id": user_ids.pop()	
				});
			}
		';

		$this->vk->getRequest()->post('execute',config('group.access_token'),[
			'code' => $code
		]);
	}

	protected function getGroupOwners()
	{
		try{
			$owners = $this->vk->groups()->getMembers(config('group.access_token',[
				'group_id' => config('group.group_id'),
				'filter' => 'managers',
				'count' => 25,
				'sort' => 'time_asc'
			]));
			
			$user_ids = [];
			foreach($owners['items'] as $key => $owner)
			{
				$user_ids[] = $owner->id;
			}
			
			return $user_ids;
		}
		catch(\Exception $e)
		{
			$this->storage->temp[] = $e->getMessage();
			return [];
		}
	}
}
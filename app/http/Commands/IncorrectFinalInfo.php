<?php
namespace App\Http\Command;

class IncorrectFinalInfo extends Command
{
    protected $message = '{{ name }}, я отправила ваш запрос своим создателям. Ожидайте. &#8987;';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Вернуться к началу',
            'color' => 'green',

            'text_payload' => 'начал',
            'goto_command' => 'StartCommand'
        ]
    ];

    public function handler()
    {
        parent::handler();
    }
}
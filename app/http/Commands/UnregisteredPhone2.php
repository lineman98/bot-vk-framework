<?php
namespace App\Http\Command;

class UnregisteredPhone2 extends Command
{
    protected $message = '{{ name }}, я отправила ваш запрос своим создателям. Ожидайте. &#8987;';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Попробовать еще раз',
            'color' => 'зеленый',
            'goto_command' => 'PhoneNumber2'
        ]
    ];

    protected function preHandler()
    {
        $this->sendReports();
    }

    public function handler()
    {
        parent::hanlder();
    }
}
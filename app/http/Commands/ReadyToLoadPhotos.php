<?php
namespace App\Http\Command;

class ReadyToLoadPhotos extends Command
{
    protected $message = '{{name}}, начальная идентификация пройдена ✅

    Следующий шаг - ваши фотографии. Я прикрепила образец фотографий, которые вы должны мне прислать 😏
    
    Когда вы сфотографируетесь, нажмите на кнопку “Готова отправить”, чтобы я их сохранила. 
    
    {{name}}, ваши фотографии увидит только тренер и никто больше. 
    
    P.S "А можно ли не прикреплять фотографии?" - нет, нельзя.';
    protected $attachment = 'photo-173426491_456239181,photo-173426491_456239180,photo-173426491_456239182';
 
    protected $buttons_row_1 = [
        [
            'name' => 'Готова отправить',
            'color' => 'зеленый',
            'goto_command' => 'SendPhotos'
        ]
    ];


    public function handler()
    {
        parent::handler();

        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
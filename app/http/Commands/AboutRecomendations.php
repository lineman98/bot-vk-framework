<?php
namespace App\Http\Command;

class AboutRecomendations extends Command
{
    protected $message = '{{name}}, чтобы добиться максимальных результатов за этот месяц, вам нужно соблюдать наши ежедневные рекомендации &#128077;

Их вы можете прочесть в группе (скриншот прикрепила) 

После того, как ознакомитесь с рекомендациям, нажмите на кнопку "Идем дальше" &#128077;';
    protected $attachment = 'photo-173489510_456239222';

    protected $buttons_row_1 = [
        [
            'name' => 'Идем дальше',
            'color' => 'green',
            'goto_command' => 'AboutQuestions'
        ] 
    ];
}
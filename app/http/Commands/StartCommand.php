<?php
namespace App\Http\Command;

class StartCommand extends Command
{
	protected $message = '{{ name }}, меня зовут Фити – я ваша персональная помощница🙎

    Рада вас приветствовать в нашей игре FITGAME! 🙂
    
    {{ name }}, чтобы начать участие в проекте, нам нужно с вами выполнить пару мелких заданий, после чего я расскажу вам о нашей группе и ее возможностях. 
    
    {{ name }}, чтобы со мной общаться, вам нужно включить виртуальную клавиатуру. (как это сделать, я показала на скриншоте.)
    
    Если вы увидели виртуальную клавиатуру, то нажмите на кнопку “Клавиатуру вижу”, так я пойму, что все хорошо, и мы пойдем дальше.💪';
    
    protected $attachment = 'photo-119012383_456239018';

    protected $buttons_row_1 = 
    [
        [
            'name' => 'Клавиатуру вижу',
            'color' => 'зеленый',
            'goto_command' => 'PhoneNumber'
        ]
    ];

	public function handler()
	{
		if(mb_ereg_match('\не вижу',$this->input->object->text))
		{
			$this->nextCommand = 'PhoneNumber';
		}

        parent::handler();
        
        $this->user->mod = $this->nextCommand == 'PhoneNumber' ?  'buttons' : 'text';
	}
}
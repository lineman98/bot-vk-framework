<?php
namespace App\Http\Command;

class AboutQuestions extends Command
{
    protected $message = '{{name}}, если у вас будут какие-то вопросы по: &#128071;
- Питанию
- Тренировкам 
- Организационным вопросам 

то смело нажимайте зеленую кнопку "Задать вопрос" 
&#128077; 

{{name}}, если все понятно, нажмите на кнопку "Идем дальше"';
    protected $attachment = 'photo-173489510_456239221';

    protected $buttons_row_1 = [
        [
            'name' => 'Идем дальше',
            'color' => 'green',
            'goto_command' => 'AboutDiscussions'
        ] 
    ];
}
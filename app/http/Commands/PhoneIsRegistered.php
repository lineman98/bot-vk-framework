<?php
namespace App\Http\Command;

class PhoneIsRegistered extends Command
{
    protected $message = '{name}, я нашла вашу заявку. Вот информация, которую я нашла: <br>Имя: Илья <br>Фамилия: Быстров <br>Номер телефона: 7 (900) 234-55-55 <br>Пакет участия: “Участница” <br>Это вы?';
    protected $attachment = '';
 
    protected $buttons_row_1 = [
        [
            'name' => 'Да, это я',
            'color' => 'зеленый',
            'goto_command' => 'ReadyToLoadPhotos'
        ],
        [
            'name' => 'Нет, это не я',
            'color' => 'красный',
            'goto_command' => 'IncorrectInfo'
        ]
    ];


    public function handler()
    {
        parent::handler();

        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
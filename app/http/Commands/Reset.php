<?php
namespace App\Http\Command;

class Reset extends Command
{
    protected $message = 'Кнопки сброшены';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Начать',
            'color' => 'blue',
            'goto_command' => 'StartCommand'
        ] 
    ];
}
<?php
namespace App\Http\Command;

class PhoneNumberRegistered extends Command
{
    protected $message = '{{name}}, по нашим данным, кто-то другой уже зарегистрирован под этим номером. 

    Сейчас вы должны будете пройти идентификацию по номеру телефона, чтобы удостовериться в правильности ваших данных, а то вдруг мои создатели напортачили 🙆
    
    {{name}}, введите в ответное сообщение свой номер телефона, строго по следующему шаблону. В противном случае, мы не сможем найти вашу заявку. 🙅
    
    Шаблон: 7 (900) 000-00-00 ';
    protected $attachment = '';

    public function handler()
    {
        if(mb_ereg_match('.*[0-9].?[0-9].?[0-9].?[0-9].?[0-9].?[0-9].?[0-9].*$',$this->input->object->text,'i'))
        {
            $phone_number = mb_substr( mb_ereg_replace('[^0-9]','', $this->input->object->text),-7,7 ) ;
            $this->user->phone_number = $phone_number;

            if(array_key_exists($phone_number, $this->storage->userinfos))
            {
                if(!array_key_exists('system',$this->storage->userinfos[$phone_number]) ||
                    !array_key_exists('vk_id',$this->storage->userinfos[$phone_number]['system']) ||
                    empty($this->storage->userinfos[$phone_number]['system']['vk_id'])
                    )
                $this->nextCommand = 'CheckUserinfo';
            }
            else 
            {
                $this->nextCommand = 'UnregisteredPhone1';
            }            
        }
        
    }
}
<?php
namespace App\Http\Command;

class ReadyToSendLengths extends Command
{
    protected $message = '{name}, все, я сохранила ваши фотографии. Следующий шаг, это замеры вашего тела. Я прикрепила скриншот и там указаны все замеры, которые нам нужны. Важно. Замеры тела нужно обязательно делать утром, иначе вы получите не корректные цифры. Когда вы все замерите, нажмите на кнопку. Что нужно замерять: 
1. Рост
2. Вес
3. Возраст
4. Обхват груди
5. Обхват талии
6. Обхват бёдер
7. Обхват левой ноги
8. Обхват правой ноги
9. Обхват левой руки
10. Обхват правой руки';
    protected $attachment = '';
 
    protected $buttons_row_1 = [
        [
            'name' => 'Я все замеряла',
            'color' => 'зеленый',
            'goto_command' => 'SendLengths'
        ]
    ];


    public function handler()
    {
        parent::handler();

        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
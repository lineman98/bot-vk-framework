<?php
namespace App\Http\Command;

class PhoneNumberHelp extends Command
{
    protected $message = '{{ name }}, я отправила ваш запрос своим создателям. Ожидайте. &#8987;';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Попробовать еще раз',
            'color' => 'blue',
            'goto_command' => 'PhoneNumber2'
        ]
    ];
 
    public function handler()
    {
        parent::handler();
    }
}
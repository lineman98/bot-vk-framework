<?php
namespace App\Http\Command;

class SMTIncorrect extends Command
{
    protected $message = 'Хм, вы хотите изменить введенную вами информацию, или введенную моими создателями?';
    protected $attachment = '';

    protected $buttons_row_1 = [
    	[
    		'name' => 'Изменить введенную мной инфомацию',
    		'color' => 'green',

            'text_payload' => 'мн'
    		'goto_command' => 'PhoneNumber'
    	]
    ];

    protected $buttons_row_2 = [
    	[
    		'name' => 'Bведенную создателями',
    		'color' => 'blue',

            'text_payload' => 'создател'
    		'goto_command' => 'IncorrectFinalInfo'
    	]
    ];

    public function handler()
    {
        parent::handler();
    
    }
}
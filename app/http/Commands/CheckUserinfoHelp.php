<?php
namespace App\Http\Command;

class CheckUserinfoHelp extends Command
{
    protected $message = '{{name}}, я отправила ваш запрос своим создателям. Ожидайте. &#8987;';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Попробовать еще раз',
            'color' => 'blue',
            'goto_command' => 'PhoneNumber'
        ]
    ];

    public function handler()
    {
        parent::handler();
    }

    public function preHandler()
    {
        $this->sendReports();
    }
}
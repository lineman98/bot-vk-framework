<?php
namespace App\Http\Command;

class AboutDiscussions extends Command
{
    protected $message = '{{name}}, если у вас пытливый ум (а мы очень надеемся), и вас мучает кучу вопросов, касательно: &#128071;
- тренировок
- питания
- диет 
- мифов о еде 

Например "А какой хлеб лучше есть? и нужен ли он вообще?" &#128064;

То для такой категории вопросов, мы сделали специальное обсуждение. (Прикрепила скриншот) 

Самые популярные вопросы мы будем освещать на стене группы. &#128170;

{{name}}, если все понятно, нажмите на кнопку "Идем дальше"';
    protected $attachment = 'photo-173489510_456239220';

    protected $buttons_row_1 = [
        [
            'name' => 'Идем дальше',
            'color' => 'green',
            'goto_command' => 'TourEnd'
        ] 
    ];
}
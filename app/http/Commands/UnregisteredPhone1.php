<?php
namespace App\Http\Command;

class UnregisteredPhone1 extends Command
{
    protected $message = '{{name}}, мы не нашли вашу заявку &#10060;

Вы уверены в корректности введенных данных или ошибка с нашей стороны? 

    "Попробовать еще раз" - ввести повторно номер телефона. 
    "Нужна помощь" - если Фити не может найти вашу заявку.';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Попробовать еще раз',
            'color' => 'blue',
            'goto_command' => 'PhoneNumber2'
        ]
    ];

    protected $buttons_row_2 = [
        [
            'name' => 'Нужна помощь',
            'color' => 'green',
            'goto_command' => 'PhoneNumberHelp'
        ]
    ];

    public function handler()
    {
        parent::handler();
        if($this->nextCommand == 'PhoneNumberHelp')
            $this->sendReports();
    }
}
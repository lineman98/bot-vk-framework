<?php
namespace App\Http\Command;

class TourStart extends Command
{
    protected $message = '{{name}}, сейчас я вам расскажу о том, что вас ждет &#128064;

Каждую субботу, я буду вам высылать индивидуальную продуктовую корзину на неделю, которая будет отвечать вашим целям на месяц &#127860;

Каждый день, начиная с воскресенья (11.11.2018), я буду высылать вам на следующий день: питание + тренировки &#128170;

{{name}}, если все понятно, нажмите на кнопку "Идем дальше" ';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Идем дальше',
            'color' => 'green',
            'goto_command' => 'AboutChat'
        ] 
    ];
}
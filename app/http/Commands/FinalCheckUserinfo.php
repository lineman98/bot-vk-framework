<?php
namespace App\Http\Command;

class FinalCheckUserinfo extends Command
{
    protected $message = 'Информация о Вас: 👇<br>';
    protected $attachment = '';

    protected $buttons_row_1 = [
        [
            'name' => 'Да, все правильно',
            'color' => 'green',
            'goto_command' => 'FormFinal'
        ] 
    ];

    public function preHandler()
    {
        foreach($this->user->userinfo['constant'] as $key => $val)
        {
            if(is_string($val) == false) continue;

            $key = ucfirst($key);
            $val = ucfirst($val);

            $this->message .= "<br>$key: $val";
        }
        foreach($this->user->userinfo['getted'] as $key => $val)
        {
            if(is_string($val) == false) continue;

            $key = ucfirst($key);
            $val = ucfirst($val);

            $this->message .= "<br>$key: $val";
        }

        foreach($this->user->userinfo['getted']['dims'] as $key => $val)
        {
            if(is_string($val) == false || mb_ereg_match('.*[0-9].*',$val) == false) continue;

            $key = ucfirst($key);
            $val = mb_ereg_replace('^[^0-9]*','',$val);

            $this->message .= "<br>$key: $val";
        }

        $this->message .= '<br>Ваши фотографии: ';
        foreach($this->user->userinfo['getted']['photos'] as $key => $val)
        {
            $key = $key+1;
            $this->message .= "<br>$key) $val";
        }

        $this->message .= '<br><br>{{ name }}, все верно? 🙂';
    }
}
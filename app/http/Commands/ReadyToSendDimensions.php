<?php
namespace App\Http\Command;

class ReadyToSendDimensions extends Command
{
    protected $message = '{{name}}, я сохранила ваши фотографии ✅

    Следующий шаг - замеры вашего тела. Я прикрепила скриншот, там указаны все замеры, которые нам нужны. 
    
    Замеры тела обязательно нужно обязательно делать утром, иначе вы получите не корректные цифры. Когда вы все замерите, нажмите на кнопку "Отправить замеры" 💪';
    protected $attachment = 'photo-173426491_456239183';
 
    protected $buttons_row_1 = [
        [
            'name' => 'Отправить замеры',
            'color' => 'green',
            'goto_command' => 'SendDimensions'
        ]
    ];


    public function handler()
    {
        parent::handler();

        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
<?php
namespace App\Http\Command;

class SendDimensions extends Command
{
    protected $message = 'Имя, напишите свои замеры в виде списка, который я вам отсылала выше👆

	Пример: 
	1) 183 
	2) 74
	3) 32 
	
	И так далее 💬';
    protected $attachment = '';



    public function handler()
    {
    	$dims = [
    		1 => 'Рост',
    		2 => 'Вес',
    		3 => 'Возраст',
    		4 => 'Обхват груди',
    		5 => 'Обхват талии',
    		6 => 'Обхват бёдер',
    		7 => 'Обхват левой ноги',
    		8 => 'Обхват правой ноги',
    		9 => 'Обхват левой руки',
    		10 => 'Обхват правой руки'
    	];

    	$found = 0;
    	foreach($dims as $dimNum => $dimName)
    	{

    		$dim = null;
    		mb_ereg_search_init($this->input->object->text, $dimNum . '[\)\.]{1}.*[1-9]{1}.*');
       		$dim = mb_ereg_search_regs($dimNum . '[\)\.]{1}.*[1-9]{1}.*','iU');
       		if(!empty($dim))
       		{
       			$found++;
       			$this->user->userinfo['getted']['dims'][$dimName] = trim(mb_substr($dim[0],2));
       		}
    	}

    	if($found >= 5)
        	$this->nextCommand = 'SelectLevel';
    
    }
}
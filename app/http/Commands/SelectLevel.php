<?php
namespace App\Http\Command;

class SelectLevel extends Command
{
    protected $message = '{{name}}, мы почти у цели 🎯 

    Давайте определимся с вашим уровнем тренировок.
    Выберите один из вариантов 👀';
    protected $attachment = '';

    protected $buttons_row_1 = [
    	[
    		'name' => 'Новичок',
    		'color' => 'синий',
    		'goto_command' => 'SetTarget'
    	],
    	[
    		'name' => 'С опытом',
    		'color' => 'синий',
    		'goto_command' => 'SetTarget'
    	]
    ];

    public function handler()
    {
        parent::handler();

        if(mb_ereg_match('Нович.{1}к',$this->input->object->text,'i'))
        {
            $this->user->userinfo['getted']['Уровень'] = 'Новичок';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'SetTarget';
        }
        else if(mb_ereg_match('С опытом',$this->input->object->text,'i'))
        {
            $this->user->userinfo['getted']['Уровень'] = 'С опытом';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'SetTarget';
        }
        
    
    }
}
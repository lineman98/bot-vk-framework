<?php
namespace App\Http\Command;

class CheckButtons extends Command
{
    protected $message = 'Отлично! Давай проверим включена ли <br>клавиатура у тебя, как на фото ниже 👇 <br>Если ты не видишь кнопки, напиши "не вижу"';
    protected $attachment = 'photo-119012383_456239018';

    protected $buttons_row_1 = 
    [
        [
            'name' => 'Клавиатуру вижу',
            'color' => 'зеленый',
            'goto_command' => 'Second'
        ]
    ];

    public function handler()
    {
        $this->nextCommand = 'SecondTextMod';

        parent::handler();
        
        $this->userinfo['mod'] = $this->nextCommand == 'SecondTextMod' ?  'text' : 'buttons';
    }
}
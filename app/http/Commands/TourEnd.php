<?php
namespace App\Http\Command;

class TourEnd extends Command
{
    protected $message = '{{name}}, на этом наше обучение закончено &#127881;

Уже в эту субботу ты получишь свою продуктовую корзину на неделю &#127860;

Не прощаемся, твоя Фити &#10084;';
    protected $attachment = '';
 
}
<?php
namespace App\Http\Command;

use App\Http\Command\Command;

class IncorrectInfo extends Command
{
    protected $message = '{{ name }}, увы, что-то пошло не так. Нажмите на кнопку “Нужна помощь”.';
    protected $attachment = '';
 
    protected $buttons_row_1 = [
        [
            'name' => 'Да, это я',
            'color' => 'красный',
            'goto_command' => 'IncorrentInfoHelp'
        ]
    ];


    public function handler()
    {
        parent::handler();

        /*if(mb_ereg_match('.*.*',$input->object->text,'i') || mb_ereg_match('1',$input->object->text))
        {
            $this->userinfo['roses_color'] = '';
            //запоминаем что выбрал пользователь
            $this->nextCommand = 'ThirdTextMod';
        }*/
    
    }
}
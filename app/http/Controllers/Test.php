<?php 

namespace App\Http\Controller;

use App\Http\Command\Command;
use Service\Storage;
use Service\User;

class Test
{

	public $storage;
	protected $user;
	protected $input;

	protected $nextCommand;


	//инициализируем контроллер
	public function __construct()
	{ 
		$this->storage = new Storage;
	}

	public function __destruct()
	{
		$this->storage->save();
	}

	/**
	 * Точка входa
	 */
	public function index()
	{ 
		return  mb_substr(mb_ereg_replace('[^0-9]','','8 (029) 309-11-22'),-7,7);


		$command = $this->getcommandInstance('StartCommand');
		$this->user = $this->getUser(123);
	}




	private function ExecCurrentCommandHandler()
	{
		$currentCommandName = $this->user['command'];
		//Создем экземпляр команды
		
		$currentCommand = $this->getCommandInstance($currentCommandName);

		//Запускем обработчик команды
		$currentCommand->handler();
		//если текущая команда указала что нужно сделать отправку данных, включаем отправщик
		if( property_exists($currentCommand, 'sendReports') && $currentCommand->sendReports == true )
			$this->sendReports();


		$this->nextCommand = $currentCommand->getNextCommand();
		if($this->nextCommand == false  ) $this->nextCommand = $currentCommandName;
	}


	/**
	 * Проверяет содержатся ли в сообщении особые слова-комманды
	 */
	protected function checkKeywordsInMessage()
	{
		if(mb_ereg_match('Начать',$this->input->object->text,'i'))
			$this->user['command'] = 'StartCommand';
		elseif(mb_ereg_match('Режим',$this->input->object->text,'i'))
			$this->user['command'] = 'CheckButtons';
	}

	/*
		Отправляет ответ пользователю содержащий текст следующей команды
	*/
	private function sendNextCommandAnswer()
	{
		//Загружаем следующую команду
		$nextCommand = $this->getCommandInstance($this->nextCommand);
		//отправляем сообщение пользователю
		$this->sendAnswer($nextCommand->getAnswer());
	}

	/*
		Возвращает объект содержащи ссылки на всю информацию о пользователе в хранилище
	*/
	public function getUser($user_id)
	{
		return new User($user_id,$this->storage);
	}


	/**
	 * Отсылает сообщение пользователю
	 */
	public function sendAnswer($answer)
	{
		$answer['user_id'] = $this->user_id;
		//if(array_key_exists('keyboard',$answer))
		$this->storage->chats[$this->user_id]['command'] = $this->nextCommand;
		
		try{
			$this->vk->messages()->send($this->storage->group['access_token'],$answer);
		}
		catch(\Exception $e)
		{
			$this->storage->temp['err'] = $e->getMessage();
		}
	}

	/*
		Загружает и возвращает экземпляр нужной команды
	*/
	public function getCommandInstance($commandName)
	{
		$commandName = 'App\\Http\\Command\\'.$commandName;

		if( class_exists($commandName) )
		{
			return new $commandName(
				$this->user,
				$this->input,
				$this->storage
			);
		}
	}


	/*
		Загружает входные данные
	*/
	private function getInput()
	{
		/*$this->input = json_decode('{
		    "type": "message_new",
		    "object": {
		        "date": 1536432618,
		        "from_id": 240597953,
		        "id": 97,
		        "out": 0,
		        "peer_id": 240597953,
		        "text": "\u041a\u0430\u043a \u043f\u0440\u043e\u0448\u043c\u044b\u0433\u043d\u0443\u0442\u044c \u0447\u0435\u0440\u0435\u0437 \u0433\u0440\u0430\u043d\u0438\u0446\u0443",
		        "conversation_message_id": 86,
		        "fwd_messages": [],
		        "important": false,
		        "random_id": 0,
		        "attachments": [],
		        "payload": "",
		        "is_hidden": false
		    },
		    "group_id": 170995635
		}');*/ 

		return (!empty(file_get_contents('php://input'))) ? 
				json_decode(file_get_contents('php://input')) : 
				false;
	}


	/*
		
	*/
	protected function sendReports()
	{
		$this->storage->orders[] = $this->generateReport();


		$groupManagers = $this->getGroupManagers();

		if($groupManagers != false)
		{	
			foreach($groupManagers['items'] as $manager)
			{
				if($manager['role'] == 'moderator')
				 	continue;
				
				$this->sendReportPeer($manager['id']);
			}

		}

		//$this->sendReportPeer(abs(config('group.id')) * -1);
	}

	protected function getGroupManagers()
	{
		try{
			return $this->vk->groups()->getMembers($this->storage->group['access_token'],[
				'group_id' => abs(config('group.id')),
				'filter' => 'managers'
			]);
		}
		catch(\Exception $e)
		{
			$this->storage->temp[] = $e->getMessage();
			return false;
		}
	}

	protected function sendReportsToUsers($user_ids)
	{
		try{
			return $this->vk->messages()->send($this->storage->group['access_token'],[
				'user_ids' => $user_ids,
				'message' => $this->generateReport()
			]);
		}
		catch(\Exception $e)
		{
			if(is_string($this->storage->temp))
				$this->storage->temp = [];
			$this->storage->temp[] = $e->getMessage();
			return false;
		}
	}

	protected function sendReportPeer($peer_id)
	{
		try{
			return $this->vk->messages()->send($this->storage->group['access_token'],[
				'peer_id' => $peer_id,
				'message' => $this->generateReport()
			]);
		}
		catch(\Exception $e)
		{
			$this->storage->temp[] = $e->getMessage() . '  --  ' . $peer_id;
			return false;
		}
	}

	protected function generateReport()
	{
		$userinfo = $this->storage->chats[$this->user_id];
		$report = "
			------------------------
			Заказ №" . count($this->storage->orders) . "
			
			Получатель букета: '" . $userinfo['recivier_type'] . "'
			Имя получателя: '" . $userinfo['recivier_name'] . "'
			Цвет роз: " . $userinfo['roses_color'] . "
			Количество роз: " . $userinfo['roses_count'] . "
			Цвет коробки: " . $userinfo['box_color'] . "
			
			Телефон заказчика: '" . $userinfo['phone_number'] . "'
			[id" . $this->user_id . "|Страница заказчика]
			------------------------
		";
		return $report;
	}
}
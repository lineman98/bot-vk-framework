<?php 
namespace App\Http\Controller;

use VK\Client\VKApiClient;
use App\Http\Command\Command;
use Service\Storage;

class SleepChecker
{
	protected $vk; //дескриптор вк апи сдк

	public $storage;
	protected $user;
	protected $input;

	protected $nextCommand;


	//инициализируем контроллер
	public function __construct()
	{ 
		$this->vk = new VKApiClient('5.85');
		
		$this->storage = new Storage;
	}

	public function __destruct()
	{
		$this->storage->save();
	}

	/**
	 * Точка входa
	 */
	public function index()
	{
		if($this->nowTimeForSleep() !== true)
			return 'not sleep time!!! ';
		
		$user_ids_for_check = [];
		foreach($this->storage->userinfos as $userinfo)
		{
			if($this->IsActiveUser($userinfo))
			{	
				$user_ids_for_check[] = $userinfo['system']['vk_id'];
			}
		}
		print_r($user_ids_for_check);
		$notSleepingUserIds = $this->getNotSleeping($user_ids_for_check);
		print_r($notSleepingUserIds);
		$this->barkToEveryone($notSleepingUserIds);
	}

	protected function nowTimeForSleep()
	{
		$nowHour = date('G');

		$to_hour = config('users.sleep.from') + config('users.sleep.length');
		if($to_hour >= 24) $to_hour -= 24;

		return($nowHour >= config('users.sleep.from') || $nowHour <= $to_hour);
	}

	protected function IsActiveUser(& $userinfo)
	{
		return (array_key_exists('system',$userinfo) && 
			    array_key_exists('active',$userinfo['system']) &&
				$userinfo['system']['active'] == true &&
				array_key_exists('vk_id', $userinfo['system'])
		);
	}

	protected function getNotSleeping($user_ids)
	{
		$notSleeping = [];

		foreach(array_chunk($user_ids,1000) as $chunk)
		{
			$result = $this->vk->users()->get(config('group.access_token'),[
				'user_ids' => implode(',', $chunk),
				'fields' => 'online'
			]);
			foreach($result as $user)
			{
				if($user['online'] == 1)
					$notSleeping[] = $user['id'];
			}
		}

		return $notSleeping;
	}

	protected function barkToEveryone($user_ids)
	{
		$barkCommand = new \App\Http\Command\BarkIfNotSleep($null,$null,$null);
		$barkAnswer = $barkCommand->getAnswer();

		foreach($user_ids as $user_id)
		{
			try
			{	$this->vk->messages()->send(config('group.access_token'),
				[
					"user_id" => $user_id,
					"message" => $barkAnswer['message'],
					"attachment" =>  $barkAnswer['attachment']
				]);
			}
			catch(\Exception $e)
			{
				$this->storage->temp[] = $e->getMessage();
			}

		}
	}
}
<?php 
namespace App\Http\Controller;

use VK\Client\VKApiClient;
use App\Http\Command\Command;
use Service\Storage;

class ScheduleSender
{
	protected $vk; //дескриптор вк апи сдк

	public $storage;
	protected $user;
	protected $input;

	protected $nextCommand;


	//инициализируем контроллер
	public function __construct()
	{ 
		$this->vk = new VKApiClient('5.85');
		
		$this->storage = new Storage;
	}

	public function __destruct()
	{
		$this->storage->save();
	}

	/**
	 * Точка входa
	 */
	public function index()
	{
		foreach($this->storage->userinfos as $key => $userinfo)
		{
			
			print_r($userinfo);
			if($this->isActiveUser($userinfo) != false)
			{

				$userinfo = $this->trySendEarlyestMessage($userinfo);
			}

			//сохраняем сделанные изменения
			$this->storage->userinfos[$key] = $userinfo;
		}
		
	}





	protected function IsActiveUser(& $userinfo)
	{
		return (array_key_exists('system',$userinfo) && 
			    array_key_exists('active',$userinfo['system']) &&
				$userinfo['system']['active'] == true &&
				array_key_exists('vk_id', $userinfo['system'])
		);
	}

	protected function checkMessagesForSend($userinfo)
	{
		foreach($userinfo['message_plan'] as $date => $message)
		{
			$dates = explode(' ', $date);
			$time = explode(':',$dates[1]);
			$date = explode('.',$dates[0]);

			$unix = mktime($time[0],$time[1],0,$date[1],$date[0],$date[2]);
			print_r($unix);
		}
	}


	protected function updateUserDay($userinfo)
	{
		if($userinfo['system']['active'] == true)
		{
			if($this->isActiveUser($userinfo) != false)
			{
				$userinfo['system']['day'] += ( date('d') >= date('d', $userinfo['system']['start_time']) ) ?
						floor( (integer)date('d') - (integer)date('d',$userinfo['system']['start_time']) ) :
						(integer)date('t',$userinfo['system']['start_time']) - (integer)date('d',$userinfo['system']['start_time']) + (integer)date('d');
				print('today is' . date('d') . ', was it ' . date('d',$userinfo['system']['start_time']));
			}

			$userinfo['system']['start_time'] = time(); 
		}

		return $userinfo;
	}


	protected function trySendEarlyestMessage($userinfo)
	{
		$plan = $userinfo['messages_plan'];
		if(count($plan) > 0)
		{
			ksort($plan);

			foreach($plan as $key => $message)
			{
				if(array_key_exists('status',$message) && $message['status'] == 'отослано')
					unset($plan[$key]);
			}

			foreach($plan as $date => $messageInfo)
			{
				preg_match('/day([0-9]+)_([0-9]{1,2})\:([0-9]{1,2})/',$date,$dates);
				//print_r($dates);
				//print('1 = ' .  $userinfo['system']['day']);
				//print('2 = ' .  date('G'));
				//print('3 = ' .  date('i'));
				if(
					(integer) $dates[1] >= $userinfo['system']['day'] && 
							  date('G') >= $dates[2] && 
							  date('i') >= $dates[3]
				)
				{

					$params = [
						'user_id' => $userinfo['system']['vk_id'],
						'message' => $messageInfo['message'],
						'attachment' => $messageInfo['attachment']
					];
					print_r($params);
					if($this->sendMessage($params) == true)
					{
						$messageInfo['status'] = 'отослано';
						$userinfo['messages_plan'][$date] = $messageInfo;
					}
				}

				break;
			}
		}

		return $userinfo;
	}

	protected function sendMessage($params)
	{
		try
		{
			$this->vk->messages()->send(config('group.access_token'),$params);
			return true;
		}
		catch(\Exception $e)
		{
			print_r($e->getMessage());
			return false;
		}
	}
}
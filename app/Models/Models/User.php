<?php
namespace App\Model;

class User extends SimpleORM
{
	protected static $table = 'users';
	protected static $pk = 'vk_id';

	public function increaseCounter()
	{
		if($this->last_message_time != date('d.m.Y') )
		{
			$this->counter = 1;
			$this->last_message_time = date('d.m.Y');
		}
		else
		{
			$this->counter++;
		}

		return $this;
	}
}
?>